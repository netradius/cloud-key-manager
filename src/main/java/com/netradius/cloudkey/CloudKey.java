/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey;

import com.netradius.cloudkey.contract.*;

import java.util.concurrent.CompletableFuture;

public interface CloudKey extends EncrytionKeyWithAAD, VersionedRetrievableEncryptionKey,
    RotatableEncryptionKey {

  @Override
  String getName();

  @Override
  CompletableFuture<byte[]> encrypt(byte[] data);

  @Override
  CompletableFuture<byte[]> decrypt(byte[] data);

  @Override
  CompletableFuture<byte[]> encrypt(byte[] data, byte[] aad);

  @Override
  CompletableFuture<byte[]> decrypt(byte[] data, byte[] aad);

  @Override
  CompletableFuture<Crypter> encrypter(byte[] aad);

  @Override
  CompletableFuture<Crypter> decrypter(byte[] aad);

  @Override
  int ciphertextLength(int plaintextLength);

  @Override
  int plaintextLength(int ciphertextLength);

  @Override
  CompletableFuture<byte[]> getKey();

  @Override
  CompletableFuture<byte[]> getKey(long version);

  @Override
  CompletableFuture<Long> getCurrentVersion();

  @Override
  CompletableFuture<long[]> getVersions();

  @Override
  CompletableFuture<Void> rotate();
}
