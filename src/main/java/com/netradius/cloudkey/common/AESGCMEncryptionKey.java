/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.common;

import com.netradius.cloudkey.contract.Crypter;
import com.netradius.cloudkey.contract.EncryptionKey;
import com.netradius.cloudkey.contract.EncrytionKeyWithAAD;
import com.netradius.cloudkey.contract.RetrievableEncryptionKey;
import com.netradius.commons.lang.ValidationHelper;
import com.netradius.cryptolib.CryptoException;
import com.netradius.cryptolib.provider.AESProvider;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public class AESGCMEncryptionKey implements EncryptionKey, EncrytionKeyWithAAD, RetrievableEncryptionKey {

  private String name;
  private AESProvider.GCM provider;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int ciphertextLength(int plaintextLength) {
    return provider.ciphertextLength(plaintextLength);
  }

  @Override
  public int plaintextLength(int ciphertextLength) {
    return provider.plaintextLength(ciphertextLength);
  }

  @Override
  public CompletableFuture<byte[]> decrypt(byte[] data) {
    return decrypt(data, null);
  }

  @Override
  public CompletableFuture<byte[]> decrypt(byte[] data, byte[] aad) {
    byte[] b = new byte[plaintextLength(data.length)];
    ByteBuffer ob = ByteBuffer.wrap(b);
    ByteBuffer ib = ByteBuffer.wrap(data);
    return decrypter(aad)
        .thenCompose(c -> c.update(ib, ob))
        .thenCompose(c -> c.complete(ob))
        .thenApply(c -> b);
  }

  @Override
  public CompletableFuture<byte[]> encrypt(byte[] data) {
    return encrypt(data, null);
  }

  @Override
  public CompletableFuture<byte[]> encrypt(byte[] data, byte[] aad) {
    byte[] b = new byte[ciphertextLength(data.length)];
    ByteBuffer ob = ByteBuffer.wrap(b);
    ByteBuffer ib = ByteBuffer.wrap(data);
    return encrypter(aad)
        .thenCompose(c -> c.update(ib, ob))
        .thenCompose(c -> c.complete(ob))
        .thenApply(c -> b);
  }

  @Override
  public CompletableFuture<Integer> encrypt(ByteChannel in, ByteChannel out) {
    return encrypt(in, out, null);
  }

  @Override
  public CompletableFuture<Integer> decrypt(ByteChannel in, ByteChannel out) {
    return decrypt(in, out, null);
  }

  @Override
  public CompletableFuture<Integer> encrypt(ByteChannel in, ByteChannel out, byte[] aad) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        return provider.encrypt(in, out, aad);
      } catch (IOException | CryptoException x) {
        throw new CompletionException(x);
      }
    });

  }

  @Override
  public CompletableFuture<Integer> decrypt(ByteChannel in, ByteChannel out, byte[] aad) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        return provider.decrypt(in, out, aad);
      } catch (IOException | CryptoException x) {
        throw new CompletionException(x);
      }
    });
  }

  @Override
  public CompletableFuture<Crypter> encrypter(byte[] aad) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        return new CrypterImpl(provider.encrypter(aad));
      } catch (CryptoException x) {
        throw new CompletionException(x);
      }
    });
  }

  @Override
  public CompletableFuture<Crypter> decrypter(byte[] aad) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        return new CrypterImpl(provider.decrypter(aad));
      } catch (CryptoException x) {
        throw new CompletionException(x);
      }
    });
  }

  @Override
  public CompletableFuture<byte[]> getKey() {
    return CompletableFuture.supplyAsync(() -> provider.keyBytes());
  }

  private static class CrypterImpl implements Crypter {

    private AESProvider.Crypter crypter;

    CrypterImpl(AESProvider.Crypter crypter) {
      this.crypter = crypter;
    }

    @Override
    public CompletableFuture<Crypter> update(ByteBuffer input, ByteBuffer output) {
      return CompletableFuture.supplyAsync(() ->{
        try {
          crypter.update(input, output);
          return this;
        } catch (CryptoException x) {
          throw new CompletionException(x);
        }
      });
    }

    @Override
    public CompletableFuture<Crypter> complete(ByteBuffer output) {
      return CompletableFuture.supplyAsync(() ->{
        try {
          crypter.complete(output);
          return this;
        } catch (CryptoException x) {
          throw new CompletionException(x);
        }
      });
    }
  }

  public static Builder builder() {
    return new BuilderImpl();
  }

  private static class BuilderImpl implements Builder {

    private boolean generate = false;
    private byte[] key;
    private String name;

    @Override
    public Builder name(String name) {
      ValidationHelper.notBlank(name, "name");
      this.name = name.trim().toLowerCase();
      return this;
    }

    @Override
    public Builder key(byte[] key) {
      ValidationHelper.notEmpty(key, "key");
      this.key = key;
      return this;
    }

    @Override
    public Builder newKey() {
      generate = true;
      return this;
    }

    @Override
    public AESGCMEncryptionKey build() throws CryptoException {
      ValidationHelper.notBlank(name, "name");
      AESGCMEncryptionKey encryptionKey = new AESGCMEncryptionKey();
      encryptionKey.name = name;
      if (generate) {
        encryptionKey.provider = AESProvider.aes128().bcProvider().gcm128();
      } else {
        ValidationHelper.notEmpty(key, "key");
        encryptionKey.provider = AESProvider.aes(key).bcProvider().gcm128();
      }
      return encryptionKey;
    }
  }

  public interface Builder {
    Builder name(String name);
    Builder key(byte[] key);
    Builder newKey();
    AESGCMEncryptionKey build() throws CryptoException;
  }
}
