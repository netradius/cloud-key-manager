/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.common;

import com.netradius.cloudkey.contract.EncryptionKey;
import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ArrayHelper;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static com.netradius.commons.lang.ValidationHelper.notEmpty;

@Slf4j
public class MultiEncryptionKey implements EncryptionKey {

  private String name;
  private Map<ByteBuffer, CompletableFuture<EncryptionKey>> keys;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public CompletableFuture<byte[]> decrypt(byte[] data) {
    notEmpty(data, "data");
    if (log.isTraceEnabled()) {
      log.trace(String.format("Decrypting [%d] data using key [%s]", data.length, name));
    }
    /*
      Decryption works like the following:
        32 bytes - read name hash
        4 bytes - read data length (long)
        bytes - read encrypted data

     */
    return CompletableFuture.supplyAsync(() -> {
      List<CompletableFuture<byte[]>> futures = new ArrayList<>();
      int pos = 0;
      do {
        byte[] hash = Arrays.copyOfRange(data, pos, pos += 32);
        int length = BitTwiddler.betoi(Arrays.copyOfRange(data, pos, pos += 4));
        byte[] encrypted = Arrays.copyOfRange(data, pos, pos += length);
        CompletableFuture<EncryptionKey> keyFuture = keys.get(ByteBuffer.wrap(hash));
        if (keyFuture == null) {
          log.warn(String.format("Missing encryption key with hash %s", BitTwiddler.tob64str(hash)));
        } else {
          if (log.isTraceEnabled()) {
            log.trace(String.format("Found encryption key for hash %s", BitTwiddler.tob64str(hash)));
          }
          futures.add(keyFuture.thenCompose((key) -> key.decrypt(encrypted)));
        }
      } while (pos < data.length);
      if (futures.isEmpty()) {
        throw new IllegalArgumentException("No encryption keys could be found for data provided");
      }
      return futures;
    }).thenCompose(CompletableFutureHelper::anyOf);
  }

  @Override
  public CompletableFuture<byte[]> encrypt(byte[] data) {
    notEmpty(data, "data");
    if (log.isTraceEnabled()) {
      log.trace(String.format("Encrypting [%d] data using key [%s]", data.length, name));
    }
    /*
      Encryption works like the following:
      for each key
        32 byte - write key name hash
        4 bytes - write data length (long)
        bytes - write encrypted data
     */

    // Get encrypted data
    @SuppressWarnings("unchecked")
    CompletableFuture<byte[]>[] futures =  keys.entrySet().stream().map((entry) -> {
      ByteBuffer providerNameHash = entry.getKey();
      return entry.getValue()
          .thenCompose(key -> key.encrypt(data))
          .thenApply(bytes -> ArrayHelper.concat(providerNameHash.array(), BitTwiddler.itobe(bytes.length), bytes));
    }).toArray(CompletableFuture[]::new);

    // Combine encrypted data
    return CompletableFuture.allOf(futures)
        .thenApply(v -> Arrays.stream(futures)
            .map(CompletableFuture::join)
            .reduce((b1, b2) -> ArrayHelper.concat(b1, b2)).orElseThrow());
  }

  public static Builder builder() {
    return new BuilderImpl();
  }

  private static class BuilderImpl implements Builder {

    private String name;
    private Map<ByteBuffer, CompletableFuture<EncryptionKey>> keys;

    @Override
    public Builder keys(Map<ByteBuffer, CompletableFuture<EncryptionKey>> keys) {
      this.keys = keys;
      return this;
    }

    @Override
    public Builder addKey(ByteBuffer providerNameHash, CompletableFuture<EncryptionKey> key) {
      if (keys == null) {
        keys = new HashMap<>();
      }
      keys.put(providerNameHash, key);
      return this;
    }

    @Override
    public Builder name(String name) {
      this.name = name;
      return this;
    }

    @Override
    public MultiEncryptionKey build() {
      MultiEncryptionKey multiEncryptionKey = new MultiEncryptionKey();
      multiEncryptionKey.keys = this.keys;
      multiEncryptionKey.name = this.name;
      return multiEncryptionKey;
    }
  }

  public interface Builder {
    Builder keys(Map<ByteBuffer, CompletableFuture<EncryptionKey>> keys);
    Builder addKey(ByteBuffer providerNameHash, CompletableFuture<EncryptionKey> key);
    Builder name(String name);
    MultiEncryptionKey build();
  }
}
