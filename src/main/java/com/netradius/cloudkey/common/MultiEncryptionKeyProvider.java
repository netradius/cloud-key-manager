/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.common;

import com.netradius.cloudkey.contract.EncryptionKey;
import com.netradius.cloudkey.contract.EncryptionKeyProvider;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.netradius.cloudkey.common.HashHelper.hash;
import static com.netradius.commons.lang.ValidationHelper.notEmpty;

public class MultiEncryptionKeyProvider implements EncryptionKeyProvider<MultiEncryptionKey> {

  private Map<ByteBuffer, EncryptionKeyProvider<EncryptionKey>> providers;

  private MultiEncryptionKeyProvider() {}

  @Override
  public CompletableFuture<MultiEncryptionKey> getKey(String name, boolean create) {
    return CompletableFuture.supplyAsync(() -> {
      Map<ByteBuffer, CompletableFuture<EncryptionKey>> keys = providers.entrySet().stream()
          .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getKey(name, create)));
      return MultiEncryptionKey.builder().name(name).keys(keys).build();
    });
  }

  public static Builder builder() {
    return new BuilderImpl();
  }

  private static class BuilderImpl implements Builder {

    private Map<ByteBuffer, EncryptionKeyProvider<EncryptionKey>> providers;

    @Override
    @SuppressWarnings("unchecked")
    public Builder providers(Map<String, ? extends EncryptionKeyProvider> providers) {
      notEmpty(providers, "providers");
      this.providers = providers.entrySet().stream().collect(Collectors.toMap(
          entry -> ByteBuffer.wrap(hash(entry.getKey())),
          Map.Entry::getValue
      ));
      return this;
    }

    @Override
    public MultiEncryptionKeyProvider build() {
      notEmpty(providers, "providers");
      MultiEncryptionKeyProvider provider = new MultiEncryptionKeyProvider();
      provider.providers = this.providers;
      return provider;
    }
  }

  public interface Builder {
    Builder providers(Map<String, ? extends EncryptionKeyProvider> providers);
    MultiEncryptionKeyProvider build();
  }
}
