/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.aws;

import com.netradius.cloudkey.contract.EncryptionKeyProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsAsyncClient;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static com.netradius.commons.lang.ValidationHelper.notNull;

import static com.netradius.cloudkey.aws.AWSKMSHelper.*;

public class AWSKMSEncryptionKeyProvider implements EncryptionKeyProvider<AWSKMSEncryptionKey> {

  private Region region;
  private Map<String, String> context;
  private Map<String, String> tags;
  private KmsAsyncClient client;

  private AWSKMSEncryptionKeyProvider() {}

  @Override
  public CompletableFuture<AWSKMSEncryptionKey> getKey(String name, boolean create) {
    if (create) {
      return findOrCreateKey(client, "alias/" + name, tags)
          .thenApply(keyId -> AWSKMSEncryptionKey.builder()
              .client(client)
              .name(name)
              .context(context)
              .awsKeyId(keyId).build());
    } else {
      return AWSKMSHelper.findKeyId(client, "alias/" + name)
          .thenApply(keyId -> keyId == null
              ? null
              : AWSKMSEncryptionKey.builder().client(client).name(name).awsKeyId(keyId).build());
    }
  }

  public Region getRegion() {
    return region;
  }

  public static Builder builder() {
    return new BuilderImpl();
  }

  private static class BuilderImpl implements Builder {

    private Region region;
    private Map<String, String> context;
    private Map<String, String> tags;

    @Override
    public Builder region(Region region) {
      notNull(region, "region");
      this.region = region;
      return this;
    }

    @Override
    public Builder context(Map<String, String> context) {
      this.context = context;
      return this;
    }

    @Override
    public Builder tags(Map<String, String> tags) {
      this.tags = tags;
      return this;
    }

    @Override
    public AWSKMSEncryptionKeyProvider build() {
      notNull(this.region, "region");
      KmsAsyncClient client = KmsAsyncClient.builder()
          .region(this.region)
          .build();
      AWSKMSEncryptionKeyProvider provider = new AWSKMSEncryptionKeyProvider();
      provider.client = client;
      provider.context = context;
      provider.tags = tags;
      provider.region = region;
      return provider;
    }
  }

  public interface Builder {
    Builder region(Region region);
    Builder context(Map<String, String> context);
    Builder tags(Map<String, String> tags);
    AWSKMSEncryptionKeyProvider build();
  }
}
