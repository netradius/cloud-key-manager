/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.aws;

import com.netradius.cloudkey.contract.EncryptionKey;
import com.netradius.cloudkey.contract.EncryptionKeyProvider;
import com.netradius.commons.lang.ValidationHelper;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;

import static com.netradius.commons.lang.ValidationHelper.notEmpty;
import static com.netradius.commons.lang.ValidationHelper.notNull;

import static com.netradius.cloudkey.aws.AWSS3Helper.*;

@Slf4j
public class AWSS3EncryptionKeyProvider implements EncryptionKeyProvider<AWSKey> {

  private EncryptionKey keyEncryptionKey;
  private Map<String, S3AsyncClient> buckets;

  @Override
  public CompletableFuture<AWSKey> getKey(String name, boolean create) {
    ValidationHelper.notBlank(name, "name");
    if (log.isTraceEnabled()) {
      log.trace(String.format("Getting key [%s] from buckets [%s]", name, String.join(",", buckets.keySet())));
    }
    return CompletableFuture.supplyAsync(() -> {
      AWSS3EncryptionKey encryptionKey = AWSS3EncryptionKey.builder()
          .buckets(buckets)
          .keyEncryptionKey(keyEncryptionKey)
          .name(name).build();
      try {
        encryptionKey.refresh().join();
      } catch (CompletionException x) {
        if (x.getCause() instanceof NoSuchKeyException && create) {
          encryptionKey.rotate().join();
        } else {
          throw new CompletionException(x.getCause());
        }
      }
      return encryptionKey;
    });
  }

  public static Builder builder() {
    return new BuilderImpl();
  }

  private static class BuilderImpl implements Builder {

    private EncryptionKey encryptionKey;
    private Map<String, S3AsyncClient> buckets;
    private Map<String, String> tags;

    @Override
    public Builder keyEncryptionKey(EncryptionKey encryptionKey) {
      notNull(encryptionKey, "keyEncryptionKey");
      this.encryptionKey = encryptionKey;
      return this;
    }

    @Override
    public Builder buckets(Map<String, Region> buckets) {
      notEmpty(buckets, "buckets");
      Map<String, S3AsyncClient> map = buckets.entrySet().stream().collect(Collectors.toMap(
          Map.Entry::getKey,
          entry -> S3AsyncClient.builder().region(entry.getValue()).build()));
      if (map.size() != buckets.size()) {
        throw new IllegalArgumentException("bucket names must be unique");
      }
      this.buckets = map;
      return this;
    }

    @Override
    public Builder tags(Map<String, String> tags) {
      this.tags = tags;
      return this;
    }

    @Override
    public AWSS3EncryptionKeyProvider build() {
      notEmpty(buckets, "buckets");
      notNull(encryptionKey, "encryptionKeyProvider");
      CompletableFuture<?>[] futures = buckets.entrySet().parallelStream()
          .map(entry -> findOrCreateBucket(entry.getValue(), entry.getKey(), tags))
          .toArray(CompletableFuture[]::new);
      AWSS3EncryptionKeyProvider provider = new AWSS3EncryptionKeyProvider();
      provider.keyEncryptionKey = encryptionKey;
      provider.buckets = buckets;
      CompletableFuture.allOf(futures).join();
      return provider;
    }
  }

  public interface Builder {
    Builder keyEncryptionKey(EncryptionKey encryptionKey);
    Builder buckets(Map<String, Region> buckets);
    Builder tags(Map<String, String> tags);
    AWSS3EncryptionKeyProvider build();
  }
}
