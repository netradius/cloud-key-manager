/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.aws;

import com.netradius.cloudkey.CloudKey;
import com.netradius.cloudkey.CloudKeyManager;
import com.netradius.cloudkey.common.MultiEncryptionKey;
import com.netradius.cloudkey.common.MultiEncryptionKeyProvider;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.regions.Region;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.netradius.cloudkey.aws.AWSRegionHelper.toRegions;
import static com.netradius.commons.lang.ValidationHelper.notBlank;
import static com.netradius.commons.lang.ValidationHelper.notEmpty;

@Slf4j
public class AWSCloudKeyManager implements CloudKeyManager {

  private AWSS3EncryptionKeyProvider encryptionKeyProvider;

  private AWSCloudKeyManager() {}

  @Override
  public CompletableFuture<CloudKey> getKey(String name, boolean create) {
    notBlank(name, "name");
    return encryptionKeyProvider.getKey(name, create)
        .thenApply(awsKey -> awsKey);
  }

  @Override
  public CompletableFuture<CloudKey> getKey(String name) {
    return getKey(name, false);
  }

  @Override
  public CompletableFuture<Void> deleteKey(String name) {
    return getKey(name).thenCompose(cloudKey -> {
      AWSS3EncryptionKey s3key = (AWSS3EncryptionKey)cloudKey;
      return s3key.delete();
    });
  }

  public static AWSCloudKeyManagerBuilder builder() {
    return new AWSCloudKeyManagerBuilderImpl();
  }

  private static class AWSCloudKeyManagerBuilderImpl implements AWSCloudKeyManagerBuilder {

    private Region[] regions;
    private String application;
    private String environment;

    @Override
    public AWSCloudKeyManagerBuilder application(String application) {
      notBlank(application, "application");
      this.application = application;
      return this;
    }

    @Override
    public AWSCloudKeyManagerBuilder environment(String environment) {
      notBlank(environment, "environment");
      this.environment = environment;
      return this;
    }

    @Override
    public AWSCloudKeyManagerBuilder regions(String... regions) {
      notEmpty(regions, "regions");
      this.regions = toRegions(regions);
      return this;
    }

    @Override
    public AWSCloudKeyManagerBuilder regions(Region... regions) {
      notEmpty(regions, "regions");
      this.regions = regions;
      return this;
    }

    private MultiEncryptionKey kek() {
      Map<String, String> context = new HashMap<>() {{
        put("application", application);
        put("environment", environment);
      }};
      Map<String, AWSKMSEncryptionKeyProvider> providers =
          Arrays.stream(regions)
              .map(region -> AWSKMSEncryptionKeyProvider.builder().region(region).context(context).tags(context).build())
              .collect(Collectors.toMap(
                  p -> application + "-" + environment + "-" + p.getRegion(),
                  p -> p));
      return MultiEncryptionKeyProvider.builder().providers(providers).build()
          .getKey(application + "-" + environment, true).join();
    }

    @Override
    public CloudKeyManager build() {
      notBlank(application, "application");
      notBlank(environment, "environment");
      notEmpty(regions, "regions");
      AWSCloudKeyManager awsCloudKeyManager = new AWSCloudKeyManager();
      awsCloudKeyManager.encryptionKeyProvider = AWSS3EncryptionKeyProvider.builder()
          .keyEncryptionKey(kek())
          .buckets(Arrays.stream(regions).collect(Collectors.toMap(
              r -> application + "-" + environment + "-" + r.toString(),
              r -> r)))
          .build();
      return awsCloudKeyManager;
    }
  }

}
