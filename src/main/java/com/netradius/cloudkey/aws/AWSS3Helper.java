/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.aws;

import com.netradius.cloudkey.common.CompletableFutureHelper;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.paginators.ListObjectsV2Publisher;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;

import static com.netradius.commons.lang.ValidationHelper.*;

@Slf4j
public class AWSS3Helper {

  private static String groom(String str, String field) {
    notBlank(str, field);
    return str.trim().toLowerCase();
  }

  private static Tagging toTagging(Map<String, String> tags) {
    List<Tag> awsTags = tags.entrySet().stream()
        .map(entry -> Tag.builder().key(entry.getKey()).value(entry.getValue()).build())
        .collect(Collectors.toList());
    return Tagging.builder().tagSet(awsTags).build();
  }

  private static Map<String, String> fromTags(List<Tag> tags) {
    return tags.stream().collect(Collectors.toMap(Tag::key, Tag::value));
  }

  public static CompletableFuture<Boolean> checkBucketExists(S3AsyncClient s3Client, String name, int maxAttempts) {
    notNull(s3Client, "s3Client");
    min(maxAttempts, 1, "maxAttempts");
    String groomed = groom(name, "name");
    HeadBucketRequest headBucketRequest = HeadBucketRequest.builder()
        .bucket(groomed)
        .build();
    return s3Client.headBucket(headBucketRequest)
        .thenApply(headBucketResponse -> true)
        .exceptionally(x -> {
          if (x.getCause() != null && x.getCause() instanceof NoSuchBucketException) {
            if (log.isTraceEnabled()) {
              log.trace(String.format("%d attempts remaining", maxAttempts - 1));
            }
            return maxAttempts == 1 ? false : checkBucketExists(s3Client, name, maxAttempts - 1).join();
          }
          throw new RuntimeException(x);
        });
  }

  public static CompletableFuture<Boolean> getBucketVersioningEnabled(S3AsyncClient s3Client, String bucket) {
    notNull(s3Client, "s3Client");
    notBlank(bucket, "bucket");
    return s3Client.getBucketVersioning(GetBucketVersioningRequest.builder().bucket(bucket).build())
        .thenApply(getBucketVersioningResponse -> getBucketVersioningResponse.status() == BucketVersioningStatus.ENABLED);
  }

  public static CompletableFuture<Void> enableBucketVersioning(S3AsyncClient s3Client, String name) {
    notNull(s3Client, "s3Client");
    String groomed = groom(name, "name");
    PutBucketVersioningRequest putBucketVersioningRequest = PutBucketVersioningRequest.builder()
        .bucket(groomed)
        .versioningConfiguration(VersioningConfiguration.builder().status("Enabled").build())
        .build();
    return s3Client.putBucketVersioning(putBucketVersioningRequest)
        .thenApply(putBucketVersioningResponse -> null);
  }

  public static CompletableFuture<Boolean> getPublicAccessBlocked(S3AsyncClient s3Client, String bucket) {
    notNull(s3Client, "s3Client");
    notBlank(bucket, "bucket");
    return s3Client.getPublicAccessBlock(GetPublicAccessBlockRequest.builder().bucket(bucket).build())
        .thenApply(getPublicAccessBlockResponse -> {
          PublicAccessBlockConfiguration config = getPublicAccessBlockResponse.publicAccessBlockConfiguration();
          return config.blockPublicPolicy() && config.blockPublicAcls() && config.ignorePublicAcls() && config.restrictPublicBuckets();
        });
  }

  public static CompletableFuture<Void> disableBucketPublicAccess(S3AsyncClient s3Client, String name) {
    notNull(s3Client, "s3Client");
    String groomed = groom(name, "name");
    PublicAccessBlockConfiguration publicAccessBlockConfiguration = PublicAccessBlockConfiguration.builder()
        .blockPublicPolicy(true)
        .blockPublicAcls(true)
        .ignorePublicAcls(true)
        .restrictPublicBuckets(true)
        .build();
    PutPublicAccessBlockRequest putPublicAccessBlockRequest = PutPublicAccessBlockRequest.builder()
        .bucket(groomed)
        .publicAccessBlockConfiguration(publicAccessBlockConfiguration)
        .build();
    return s3Client.putPublicAccessBlock(putPublicAccessBlockRequest)
        .thenApply(putPublicAccessBlockResponse -> null);
  }

  public static CompletableFuture<Void> tagBucket(S3AsyncClient s3Client, String name, Map<String, String> tags) {
    notNull(s3Client, "s3Client");
    String groomed = groom(name, "name");
    notEmpty(tags, "tags");
    Tagging tagging = toTagging(tags);
    PutBucketTaggingRequest putBucketTaggingRequest  = PutBucketTaggingRequest.builder()
        .tagging(tagging)
        .bucket(groomed)
        .build();
    return s3Client.putBucketTagging(putBucketTaggingRequest)
        .thenApply(putBucketTaggingResponse -> null);

  }

  public static CompletableFuture<Void> tagBuckets(Map<String, S3AsyncClient> buckets, Map<String, String> tags) {
    notEmpty(buckets, "buckets");
    notEmpty(tags, "tags");
    CompletableFuture<?>[] futures = buckets.entrySet().parallelStream()
        .map(entry -> tagBucket(entry.getValue(), entry.getKey(), tags))
        .toArray(CompletableFuture[]::new);
    return CompletableFuture.allOf(futures);
  }

  public static CompletableFuture<Void> createBucket(S3AsyncClient s3Client, String name, Map<String, String> tags) {
    notNull(s3Client, "s3Client");
    String groomed = groom(name, "name");

    CreateBucketRequest createBucketRequest = CreateBucketRequest.builder()
        .bucket(groomed)
        .acl("private")
        .objectLockEnabledForBucket(true)
        .build();
    return s3Client.createBucket(createBucketRequest)
        .thenApply(createBucketResponse -> {
          boolean exists = checkBucketExists(s3Client, groomed, 20).join();
          if (exists) {
            enableBucketVersioning(s3Client, groomed).join();
            disableBucketPublicAccess(s3Client, groomed).join();
            if (tags != null && !tags.isEmpty()) {
              tagBucket(s3Client, name, tags).join();
            }
            return null;
          } else {
            throw new RuntimeException(String.format("S3 bucket %s not found", groomed));
          }
        });
  }

  public static CompletableFuture<Void> findOrCreateBucket(S3AsyncClient s3Client, String name, Map<String, String> tags) {
    notNull(s3Client, "s3Client");
    String groomed = groom(name, "name");
    return checkBucketExists(s3Client, groomed, 1)
        .thenApply(exits -> {
          if (!exits) {
            createBucket(s3Client, groomed, tags).join();
          }
          return null;
        });
  }

  public static CompletableFuture<Void> putObject(S3AsyncClient s3Client, String bucket, String key,
      Map<String, String> tags, byte[] data) {
    notNull(s3Client, "s3Client");
    String groomedBucket = groom(bucket, "bucket");
    String groomedKey = groom(key, "key");
    notEmpty(data, "data");
    PutObjectRequest.Builder putObjectRequestBuilder = PutObjectRequest.builder()
        .bucket(groomedBucket)
        .contentType("application/octet-stream")
        .key(groomedKey);
    if (tags != null && !tags.isEmpty()) {
      putObjectRequestBuilder.tagging(toTagging(tags));
    }
    PutObjectRequest putObjectRequest = putObjectRequestBuilder.build();
    AsyncRequestBody asyncRequestBody = AsyncRequestBody.fromBytes(data);
    return s3Client.putObject(putObjectRequest, asyncRequestBody)
        .thenApply(putObjectResponse -> null);
  }

  public static CompletableFuture<byte[]> getObject(S3AsyncClient s3Client, String bucket, String key) {
    notNull(s3Client, "s3Client");
    String groomedBucket = groom(bucket, "bucket");
    String groomedKey = groom(key, "key");
    if (log.isTraceEnabled()) {
      log.trace(String.format("Getting object [%s] from S3 bucket [%s]", groomedKey, groomedBucket));
    }
    GetObjectRequest getObjectRequest = GetObjectRequest.builder()
        .bucket(groomedBucket)
        .key(groomedKey)
        .build();
    return s3Client.getObject(getObjectRequest, AsyncResponseTransformer.toBytes())
        .thenApply(ResponseBytes::asByteArray);
  }

  public static CompletableFuture<Void> putObject(Map<String, S3AsyncClient> buckets, String key,
      Map<String, String> tags, byte[] data) {
    notEmpty(buckets, "buckets");
    String groomedKey = groom(key, "key");
    CompletableFuture<?>[] futures = buckets.entrySet().parallelStream()
        .map(entry -> putObject(entry.getValue(), entry.getKey(), groomedKey, tags, data))
        .toArray(CompletableFuture[]::new);
    return CompletableFuture.allOf(futures);
  }

  public static CompletableFuture<Map<String, String>> getObjectTags(S3AsyncClient client, String bucket, String key) {
    notNull(client, "client");
    notBlank(bucket, "bucket");
    String groomedKey = groom(key, "key");
    return client.getObjectTagging(GetObjectTaggingRequest.builder().bucket(bucket).key(groomedKey).build())
        .thenApply(getObjectTaggingResponse -> fromTags(getObjectTaggingResponse.tagSet()));
  }

  public static CompletableFuture<Void> putObjectTags(S3AsyncClient client, String bucket, String key, Map<String, String> tags) {
    notNull(client, "client");
    notEmpty(tags, "tags");
    String groomedKey = groom(key, "key");
    return client.putObjectTagging(PutObjectTaggingRequest.builder().bucket(bucket).key(groomedKey).tagging(toTagging(tags)).build())
        .thenApply(putObjectTaggingResponse -> null);
  }

  public static CompletableFuture<Void> putObjectTags(Map<String, S3AsyncClient> buckets, String key, Map<String, String> tags) {
    notEmpty(buckets, "buckets");
    notEmpty(tags, "tags");
    String groomedKey = groom(key, "key");
    CompletableFuture<?>[] futures = buckets.entrySet().parallelStream()
        .map(entry -> putObjectTags(entry.getValue(), entry.getKey(), groomedKey, tags))
        .toArray(CompletableFuture[]::new);
    return CompletableFuture.allOf(futures);
  }

  public static CompletableFuture<Void> addObjectTags(S3AsyncClient client, String bucket, String key, Map<String, String> tags) {
    notNull(client, "client");
    notBlank(bucket, "bucket");
    String groomedKey = groom(key, "key");
    return getObjectTags(client, bucket, groomedKey).thenApply(t -> {
      t.putAll(tags);
      return putObjectTags(client, bucket, groomedKey, t);
    }).thenApply(p -> null);
  }

  public static CompletableFuture<Void> addObjectTags(Map<String, S3AsyncClient> buckets, String key, Map<String, String> tags) {
    notEmpty(buckets, "buckets");
    notEmpty(tags, "tags");
    String groomedKey = groom(key, "key");
    CompletableFuture<?>[] futures = buckets.entrySet().parallelStream()
        .map(entry -> addObjectTags(entry.getValue(), entry.getKey(), groomedKey, tags))
        .toArray(CompletableFuture[]::new);
    return CompletableFuture.allOf(futures);
  }

  public static CompletableFuture<byte[]> getObject(Map<String, S3AsyncClient> buckets, String key) {
    notEmpty(buckets, "buckets");
    String groomedKey = groom(key, "key");
    List<CompletableFuture<byte[]>> futures = buckets.entrySet().parallelStream()
        .map(entry -> getObject(entry.getValue(), entry.getKey(), groomedKey))
        .collect(Collectors.toList());
    return CompletableFutureHelper.anyOf(futures);
  }

  public static CompletableFuture<Void> deleteObject(S3AsyncClient client, String bucket, String key) {
    notNull(client, "client");
    notBlank(bucket, "bucket");
    String groomedKey = groom(key, "key");
    if (log.isTraceEnabled()) {
      log.trace(String.format("Deleting key [%s] from bucket [%s]", groomedKey, bucket));
    }
    return client.deleteObject(DeleteObjectRequest.builder().bucket(bucket).key(groomedKey).build())
        .thenApply(deleteObjectResponse -> null);
  }

  public static CompletableFuture<Void> deleteObject(Map<String, S3AsyncClient> buckets, String key) {
    notEmpty(buckets, "buckets");
    String groomedKey = groom(key, "key");
    CompletableFuture<?>[] futures = buckets.entrySet().parallelStream()
        .map(entry -> deleteObject(entry.getValue(), entry.getKey(), groomedKey))
        .toArray(CompletableFuture[]::new);
    return CompletableFuture.allOf(futures);
  }

  public static CompletableFuture<Void> deleteObjects(S3AsyncClient client, String bucket, String prefix) {
    notNull(client, "client");
    notBlank(bucket, "bucket");
    String groomedPrefix = groom(prefix, "prefix");
    ListObjectsV2Publisher publisher = client.listObjectsV2Paginator(ListObjectsV2Request.builder().bucket(bucket).prefix(groomedPrefix).build());
    return CompletableFuture.supplyAsync(() -> {
      final CompletableFuture<Void> completableFuture = new CompletableFuture<>();
      publisher.subscribe(res -> {
        CompletableFuture<?>[] futures = res.contents().parallelStream()
            .map(s3Object -> deleteObject(client, bucket, s3Object.key()))
            .toArray(CompletableFuture[]::new);
        try {
          CompletableFuture.allOf(futures).join();
        } catch (CompletionException x) {
          completableFuture.completeExceptionally(x.getCause());
        }
      }).join();
      if (!completableFuture.isDone()) {
        completableFuture.complete(null);
      }
      return completableFuture;
    }).thenApply(CompletableFuture::join);
  }

  public static CompletableFuture<Void> deleteObjects(Map<String, S3AsyncClient> buckets, String prefix) {
    notEmpty(buckets, "buckets");
    String groomedPrefix = groom(prefix, "prefix");
    CompletableFuture<?>[] futures = buckets.entrySet().parallelStream()
        .map(entry -> deleteObjects(entry.getValue(), entry.getKey(), groomedPrefix))
        .toArray(CompletableFuture[]::new);
    return CompletableFuture.allOf(futures);
  }

}
