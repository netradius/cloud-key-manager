/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.aws;

import com.netradius.cloudkey.common.AESGCMEncryptionKey;
import com.netradius.cloudkey.contract.Crypter;
import com.netradius.cloudkey.contract.EncryptionKey;
import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ArrayHelper;
import com.netradius.commons.lang.ValidationHelper;
import com.netradius.cryptolib.CryptoException;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import software.amazon.awssdk.services.s3.S3AsyncClient;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import static com.netradius.cloudkey.aws.AWSS3Helper.*;

@Slf4j
public class AWSS3EncryptionKey implements AWSKey {

  private static final int VERSION_BYTES = 8;
  private static final String PREFIX = "keys/";
  private EncryptionKey keyEncryptionKey;
  private Map<String, S3AsyncClient> buckets;
  private String name;
  private AESGCMEncryptionKey currentKey;
  private long currentVersion;

  // Get encrypted key
  protected CompletableFuture<Pair<Long, byte[]>> getKey(Long version) {
    String prefix = PREFIX + name;
    if (version != null) {
      prefix = prefix + "/" + version;
    }
    if (log.isTraceEnabled()) {
      log.trace(String.format("Getting object [%s] from S3 buckets [%s]", prefix, String.join(",", buckets.keySet())));
    }
    return getObject(buckets, prefix)
        .thenApply(data ->
            new Pair<>(
                BitTwiddler.betol(Arrays.copyOfRange(data, 0, 8)),
                Arrays.copyOfRange(data, 8, data.length)));
  }

  // Get decrypted key
  protected CompletableFuture<Pair<Long, AESGCMEncryptionKey>> getDecryptedKey(Long version) {
    return getKey(version)
        .thenCompose(pair -> keyEncryptionKey.decrypt(pair.getValue1())
            .thenApply(decryptedKey -> new Pair<>(pair.getValue0(), decryptedKey)))
        .thenApply(pair -> {
          try {
            AESGCMEncryptionKey encryptionKey = AESGCMEncryptionKey.builder()
                .name(name)
                .key(pair.getValue1())
                .build();
            return new Pair<>(pair.getValue0(), encryptionKey);
          } catch (CryptoException x) {
            throw new CompletionException(x);
          }
        });
  }

  // Store key
  protected CompletableFuture<Void> storeKey(long version, byte[] decryptedKey) {
    if (log.isTraceEnabled()) {
      log.trace(String.format("Storing key [%s] version [%d] of length [%d]", name, version, decryptedKey.length));
    }
    return keyEncryptionKey.encrypt(decryptedKey)
        .thenCompose(encryptedKey -> {
              byte[] data = ArrayHelper.concat(BitTwiddler.ltobe(version), encryptedKey);
              return putObject(buckets, PREFIX + name + "/" + version, Collections.singletonMap("key-type", "aes-128-gcm-128"), data)
                  .thenCompose(v -> putObject(buckets, PREFIX + name, Collections.singletonMap("key-type", "aes-128-gcm-128"), data));
            });
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public CompletableFuture<byte[]> encrypt(byte[] data) {
    return encrypt(data, null);
  }

  @Override
  public CompletableFuture<byte[]> decrypt(byte[] data) {
    return decrypt(data, null);
  }

  @Override
  public CompletableFuture<byte[]> encrypt(byte[] data, byte[] aad) {
    byte[] b = new byte[ciphertextLength(data.length)];
    ByteBuffer ob = ByteBuffer.wrap(b);
    ByteBuffer ib = ByteBuffer.wrap(data);
    return encrypter(aad)
        .thenCompose(c -> c.update(ib, ob))
        .thenCompose(c -> c.complete(ob))
        .thenApply(c -> b);
  }

  @Override
  public CompletableFuture<byte[]> decrypt(byte[] data, byte[] aad) {
    byte[] b = new byte[plaintextLength(data.length)];
    ByteBuffer ob = ByteBuffer.wrap(b);
    ByteBuffer ib = ByteBuffer.wrap(data);
    return decrypter(aad)
        .thenCompose(c -> c.update(ib, ob))
        .thenCompose(c -> c.complete(ob))
        .thenApply(c -> b);
  }

  @Override
  public CompletableFuture<Integer> encrypt(ByteChannel in, ByteChannel out) {
    return encrypt(in, out, null);
  }

  @Override
  public CompletableFuture<Integer> decrypt(ByteChannel in, ByteChannel out) {
    return decrypt(in, out, null);
  }

  @Override
  public CompletableFuture<Integer> encrypt(ByteChannel in, ByteChannel out, byte[] aad) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        byte[] versionBytes = BitTwiddler.ltobe(currentVersion);
        return out.write(ByteBuffer.wrap(versionBytes));
      } catch (IOException x) {
        throw new CompletionException(x);
      }
    }).thenCompose(written -> currentKey.encrypt(in, out, aad).thenApply(i -> i + written));
  }

  @Override
  public CompletableFuture<Integer> decrypt(ByteChannel in, ByteChannel out, byte[] aad) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        byte[] buf = new byte[8];
        in.read(ByteBuffer.wrap(buf));
        return BitTwiddler.betol(buf);
      } catch (IOException x) {
        throw new CompletionException(x);
      }
    }).thenCompose((version) -> {
          CompletableFuture<Pair<Long, AESGCMEncryptionKey>> future = version == currentVersion
              ? CompletableFuture.supplyAsync(() -> new Pair<>(currentVersion, currentKey))
              : getDecryptedKey(version);
          return future.thenCompose(pair -> pair.getValue1().decrypt(in, out, aad));
        });
  }

  @Override
  public CompletableFuture<Crypter> encrypter(byte[] aad) {
    return CompletableFuture.supplyAsync(() -> new CrypterImpl(this, CrypterImpl.OpMode.ENCRYPT, aad));
  }

  @Override
  public CompletableFuture<Crypter> decrypter(byte[] aad) {
    return CompletableFuture.supplyAsync(() -> new CrypterImpl(this, CrypterImpl.OpMode.DECRYPT, aad));
  }

  @Override
  public int ciphertextLength(int plaintextLength) {
    return currentKey.ciphertextLength(plaintextLength) + VERSION_BYTES;
  }

  @Override
  public int plaintextLength(int ciphertextLength) {
    return currentKey.plaintextLength(ciphertextLength - VERSION_BYTES);
  }

  @Override
  public CompletableFuture<byte[]> getKey(long version) {
    return getDecryptedKey(version).thenCompose(pair -> pair.getValue1().getKey());
  }

  @Override
  public CompletableFuture<byte[]> getKey() {
    return currentKey.getKey();
  }

  @Override
  public CompletableFuture<Long> getCurrentVersion() {
    return CompletableFuture.supplyAsync(() -> currentVersion);
  }

  @Override
  public CompletableFuture<long[]> getVersions() {
    return null;
  }

  @Override
  public CompletableFuture<Void> rotate() {
    if (log.isTraceEnabled()) {
      log.trace(String.format("Rotating key [%s] in buckets [%s]", name, String.join(",", buckets.keySet())));
    }
    return CompletableFuture.supplyAsync(() -> {
      try {
        AESGCMEncryptionKey newKey = AESGCMEncryptionKey.builder().newKey().name(name).build();
        long newVersion = System.currentTimeMillis();
        return newKey.getKey()
            .thenCompose(decryptedKey -> storeKey(newVersion, decryptedKey))
            .thenRun(() -> {
              this.currentKey = newKey;
              this.currentVersion = newVersion;
              if (log.isTraceEnabled()) {
                log.trace(String.format("Rotated key [%s] has new version [%d]", name, newVersion));
              }
            }).join();
      } catch (CryptoException x) {
        throw new CompletionException(x);
      }
    });
  }

  protected CompletableFuture<Void> delete() {
    if (log.isTraceEnabled()) {
      log.trace(String.format("Marking key [%s] in buckets [%s] as deleted", name, String.join(",", buckets.keySet())));
    }
    return deleteObjects(buckets, PREFIX + name);
  }

  protected CompletableFuture<Void> refresh() {
    if (log.isTraceEnabled()) {
      log.trace(String.format("Refreshing key [%s] from buckets [%s]", name, String.join(",", buckets.keySet())));
    }
    return getDecryptedKey(null).thenApply(pair -> {
      this.currentVersion = pair.getValue0();
      this.currentKey = pair.getValue1();
      if (log.isTraceEnabled()) {
        log.trace(String.format("Refreshed key [%s] with version [%d]", name, this.currentVersion));
      }
      return null;
    });
  }

  private static class CrypterImpl implements Crypter {

    enum OpMode {
      ENCRYPT,
      DECRYPT
    }

    private OpMode mode;
    private AWSS3EncryptionKey parent;
    private Crypter crypter;
    private byte[] aad;

    CrypterImpl(AWSS3EncryptionKey key, OpMode mode, byte[] aad) {
      this.mode = mode;
      this.parent = key;
      this.aad = aad;
    }

    private CompletableFuture<Crypter> init(byte[] aad, ByteBuffer input, ByteBuffer output) {
      CompletableFuture<Crypter> future;
      if (mode == OpMode.ENCRYPT) {
        // TODO Need to validate there is enough output
        // Write version to output
        output.put(BitTwiddler.ltobe(parent.currentVersion));
        future = parent.currentKey.encrypter(aad);
      } else { // Decrypt
        // Read version from input
        // TODO Need to validate there is enough input
        byte[] versionBytes = new byte[8];
        input.get(versionBytes);
        long version = BitTwiddler.betol(versionBytes);
        if (parent.currentVersion == version) {
          future =  parent.currentKey.decrypter(aad);
        } else {
          // TODO Need to deal with a version that can't be found
          future = parent.getDecryptedKey(version).thenCompose((pair) -> pair.getValue1().decrypter(aad));
        }
      }
      return future;
    }

    @Override
    public CompletableFuture<Crypter> update(ByteBuffer input, ByteBuffer output) {
      if (crypter == null) {
        return init(aad, input, output).thenCompose((crypter) -> {
          this.crypter = crypter;
          return crypter.update(input, output).thenApply((c) -> this);
        });
      } else {
        return crypter.update(input, output).thenApply((c) -> this);
      }
    }

    @Override
    public CompletableFuture<Crypter> complete(ByteBuffer output) {
      return crypter.complete(output).thenApply((c) -> this);
    }
  }

  public static Builder builder() {
    return new BuilderImpl();
  }

  private static class BuilderImpl implements Builder {

    private EncryptionKey keyEncryptionKey;
    private Map<String, S3AsyncClient> buckets;
    private String name;


    @Override
    public Builder keyEncryptionKey(EncryptionKey keyEncryptionKey) {
      ValidationHelper.notNull(keyEncryptionKey, "keyEncryptionKey");
      this.keyEncryptionKey = keyEncryptionKey;
      return this;
    }

    @Override
    public Builder buckets(Map<String, S3AsyncClient> buckets) {
      ValidationHelper.notEmpty(buckets, "buckets");
      this.buckets = buckets;
      return this;
    }

    @Override
    public Builder name(String name) {
      ValidationHelper.notBlank(name, "name");
      this.name = name.trim();
      return this;
    }

    @Override
    public AWSS3EncryptionKey build() {
      ValidationHelper.notEmpty(buckets, "buckets");
      ValidationHelper.notBlank(name, "name");
      AWSS3EncryptionKey s3EncryptionKey = new AWSS3EncryptionKey();
      s3EncryptionKey.keyEncryptionKey = keyEncryptionKey;
      s3EncryptionKey.buckets = buckets;
      s3EncryptionKey.name = name;
      return s3EncryptionKey;
    }
  }

  public interface Builder {
    Builder keyEncryptionKey(EncryptionKey keyEncryptionKey);
    Builder buckets(Map<String, S3AsyncClient> buckets);
    Builder name(String name);
    AWSS3EncryptionKey build();
  }
}
