/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.aws;

import com.netradius.cloudkey.contract.EncryptionKey;
import com.netradius.commons.lang.ValidationHelper;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.kms.KmsAsyncClient;
import software.amazon.awssdk.services.kms.model.DecryptRequest;
import software.amazon.awssdk.services.kms.model.EncryptRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static com.netradius.commons.lang.ValidationHelper.notBlank;
import static com.netradius.commons.lang.ValidationHelper.notNull;

@Slf4j
public class AWSKMSEncryptionKey implements EncryptionKey {

  private String name;
  private String awsKeyId;
  private KmsAsyncClient client;
  private Map<String, String> context;

  private AWSKMSEncryptionKey() {}

  @Override
  public String getName() {
    return name;
  }

  public String getAwsKeyId() {
    return awsKeyId;
  }

  private Map<String, String> mergeContext(Map<String, String> additionalContext) {
    final Map<String, String> mergedContext;
    if (additionalContext != null && !additionalContext.isEmpty()) {
      mergedContext = new HashMap<>(context.size() + additionalContext.size());
      mergedContext.putAll(context);
      mergedContext.putAll(additionalContext);
    } else {
      mergedContext = context;
    }
    return mergedContext;
  }

  @Override
  public CompletableFuture<byte[]> decrypt(byte[] data) {
    return decrypt(data, null);
  }

  public CompletableFuture<byte[]> decrypt(byte[] data, Map<String, String> additionalContext) {
    ValidationHelper.notEmpty(data, "data");
    if (log.isTraceEnabled()) {
      log.trace(String.format("Decrypting [%d] data using key [%s]", data.length, name));
    }
    DecryptRequest decryptRequest = DecryptRequest.builder()
        .ciphertextBlob(SdkBytes.fromByteArray(data))
        .encryptionContext(mergeContext(mergeContext(additionalContext)))
        .build();
    return client.decrypt(decryptRequest).thenApply(decryptResponse -> decryptResponse.plaintext().asByteArray());
  }

  @Override
  public CompletableFuture<byte[]> encrypt(byte[] data) {
    return encrypt(data, null);
  }

  public CompletableFuture<byte[]> encrypt(byte[] data, Map<String, String> additionalContext) {
    ValidationHelper.notEmpty(data, "data");
    if (log.isTraceEnabled()) {
      log.trace(String.format("Encrypting [%d] data using key [%s]", data.length, name));
    }
    EncryptRequest encryptRequest = EncryptRequest.builder()
        .keyId(awsKeyId)
        .plaintext(SdkBytes.fromByteArray(data))
        .encryptionContext(mergeContext(additionalContext))
        .build();
    return client.encrypt(encryptRequest).thenApply(encryptResponse -> encryptResponse.ciphertextBlob().asByteArray());
  }

  public static Builder builder() {
    return new BuilderImpl();
  }

  private static class BuilderImpl implements Builder {

    private String name;
    private String awsKeyId;
    private KmsAsyncClient client;
    private Map<String, String> context;

    @Override
    public Builder name(String name) {
      notBlank(name, "name");
      this.name = name;
      return this;
    }

    @Override
    public Builder awsKeyId(String awsKeyId) {
      notBlank(awsKeyId, "awsKeyId");
      this.awsKeyId = awsKeyId;
      return this;
    }

    @Override
    public Builder client(KmsAsyncClient client) {
      notNull(client, "client");
      this.client = client;
      return this;
    }

    @Override
    public Builder context(Map<String, String> context) {
      this.context = context;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public AWSKMSEncryptionKey build() {
      notBlank(name, "name");
      notNull(client, "client");
      notBlank(awsKeyId, "awsKeyId");
      AWSKMSEncryptionKey kmsEncryptionKey = new AWSKMSEncryptionKey();
      kmsEncryptionKey.name = name;
      kmsEncryptionKey.client = client;
      kmsEncryptionKey.awsKeyId = awsKeyId;
      kmsEncryptionKey.context = context;
      return kmsEncryptionKey;
    }
  }

  public interface Builder {
    Builder name(String name);
    Builder awsKeyId(String awsKeyId);
    Builder client(KmsAsyncClient client);
    Builder context(Map<String, String> context);
    AWSKMSEncryptionKey build();
  }

}
