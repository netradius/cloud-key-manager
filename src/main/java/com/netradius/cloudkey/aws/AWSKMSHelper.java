/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey.aws;

import com.netradius.commons.lang.ValidationHelper;
import org.javatuples.Triplet;
import software.amazon.awssdk.services.kms.KmsAsyncClient;
import software.amazon.awssdk.services.kms.model.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class AWSKMSHelper {

  private static String groomAlias(String alias) {
    ValidationHelper.notBlank(alias, "alias");
    String groomed = alias.trim().toLowerCase();
    if (!groomed.startsWith("alias/")) {
      throw new IllegalArgumentException("alias must start with \"alias/\"");
    }
    return groomed;
  }

  public static CompletableFuture<Map<String, String>> getTags(KmsAsyncClient kmsClient, String keyId) {
    return kmsClient.listResourceTags(ListResourceTagsRequest.builder().keyId(keyId).build())
        .thenApply(listResourceTagsResponse -> listResourceTagsResponse.tags()
            .stream().collect(Collectors.toMap(Tag::tagKey, Tag::tagValue)));
  }

  public static CompletableFuture<Boolean> getKeyRotationEnabled(KmsAsyncClient kmsClient, String keyId) {
    return kmsClient.getKeyRotationStatus(GetKeyRotationStatusRequest.builder().keyId(keyId).build())
        .thenApply(GetKeyRotationStatusResponse::keyRotationEnabled);
  }

  public static CompletableFuture<String> findKeyId(KmsAsyncClient kmsClient, String alias) {
    ValidationHelper.notNull(kmsClient, "kmsClient");
    final String groomedAlias = groomAlias(alias);
    return kmsClient.listAliases().thenApply(
        listAliasesResponse -> listAliasesResponse.aliases().stream()
            .filter(aliasListEntry -> aliasListEntry.aliasName().equals(groomedAlias))
            .map(AliasListEntry::targetKeyId)
            .findFirst().orElse(null));
  }

  @SuppressWarnings("unchecked")
  public static List<Tag> toTags(Map<String, String> tags) {
    if (tags != null) {
      return tags.entrySet().stream()
          .map(entry -> Tag.builder().tagKey(entry.getKey()).tagValue(entry.getValue()).build())
          .collect(Collectors.toList());
    }
    return Collections.EMPTY_LIST;
  }

  public static CompletableFuture<CreateAliasResponse> createAlias(
      KmsAsyncClient kmsClient, String keyId, String alias) {
    ValidationHelper.notNull(kmsClient, "kmsClient");
    ValidationHelper.notBlank(keyId, keyId);
    final String groomedAlias = groomAlias(alias);
    CreateAliasRequest createAliasRequest = CreateAliasRequest.builder()
        .aliasName(groomedAlias)
        .targetKeyId(keyId)
        .build();
    return kmsClient.createAlias(createAliasRequest);
  }

  public static CompletableFuture<EnableKeyRotationResponse> enableKeyRotation(
      KmsAsyncClient kmsClient, String keyId) {
    ValidationHelper.notNull(kmsClient, "kmsClient");
    ValidationHelper.notBlank(keyId, "keyId");
    EnableKeyRotationRequest enableKeyRotationRequest = EnableKeyRotationRequest.builder()
        .keyId(keyId)
        .build();
    return kmsClient.enableKeyRotation(enableKeyRotationRequest);
  }

  public static CompletableFuture<Triplet<CreateKeyResponse, EnableKeyRotationResponse, CreateAliasResponse>> createKey(
      KmsAsyncClient kmsClient, String alias, Map<String, String> tags) {
    ValidationHelper.notNull(kmsClient, "kmsClient");
    String groomedAlias = groomAlias(alias);
    CreateKeyRequest createKeyRequest = CreateKeyRequest.builder()
        .tags(toTags(tags))
        .build();
    return kmsClient.createKey(createKeyRequest)
        .thenCompose(createKeyResponse ->
            enableKeyRotation(kmsClient, createKeyResponse.keyMetadata().keyId())
                .thenCompose(enableKeyRotationResponse ->
                    createAlias(kmsClient, createKeyResponse.keyMetadata().keyId(), groomedAlias)
                        .thenApply(createAliasResponse ->
                            new Triplet<>(createKeyResponse, enableKeyRotationResponse, createAliasResponse))));
  }

  public static CompletableFuture<String> findOrCreateKey(
      KmsAsyncClient kmsClient, String alias, Map<String, String> tags) {
    ValidationHelper.notNull(kmsClient, "kmsClient");
    String groomedAlias = groomAlias(alias);
    return findKeyId(kmsClient, groomedAlias)
        .thenCompose(keyId -> keyId == null
            ? createKey(kmsClient, groomedAlias, tags).thenApply(triplet -> triplet.getValue0().keyMetadata().keyId())
            : CompletableFuture.supplyAsync(() -> keyId));
  }
}
