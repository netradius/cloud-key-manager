/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cloudkey;

import com.netradius.cloudkey.aws.AWSKMSHelper;
import com.netradius.cloudkey.aws.AWSS3Helper;
import com.netradius.commons.digest.DigestHelper;
import com.netradius.commons.util.RandomHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsAsyncClient;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;

import java.io.IOException;
import java.nio.channels.ByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.concurrent.CompletionException;

import static java.lang.System.out;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@Slf4j
@TestMethodOrder(OrderAnnotation.class)
public class AWSCloudKeyManagerTest {

  private static final String QUOTE = "Any man can overcome adversity. If you truly want to test a man's character, give him power.";
  /*
    - 8 byte long (millis since epoch)
    - 32 byte hash * 2
    - 4 byte length * 2
    - 168 bytes (encrypted 16 byte AES key) * 2
    = 416
   */
  private static final int DEK_LENGTH = 416;

  private CloudKeyManager getHappyCloudKeyManger() {
    return CloudKeyManager.aws()
        .application("cloudkey-manager")
        .environment("unittest")
        .regions("us-west-2", "us-east-1")
        .build();
  }

  @Test
  @Order(0)
  public void testLengths() {
    CloudKeyManager cloudKeyManager = getHappyCloudKeyManger();

    int plaintextLength = QUOTE.getBytes(UTF_8).length;
    // plaintextLength + VERSION (8 bytes) + IV (12 bytes) + tag (16 bytes)
    int ciphertextLength = plaintextLength + 8 + 12 + 16;

    CloudKey cloudKey1 = cloudKeyManager.getKey("UnitTest", true).join();
    int ciphertextLength2 = cloudKey1.ciphertextLength(plaintextLength);
    assertThat(ciphertextLength2, equalTo(ciphertextLength));
    int plaintextLength2 = cloudKey1.plaintextLength(ciphertextLength);
    assertThat(plaintextLength2, equalTo(plaintextLength));
  }

  @Test
  @Order(1)
  public void happyPath() {
    CloudKeyManager cloudKeyManager = getHappyCloudKeyManger();

    CloudKey cloudKey1 = cloudKeyManager.getKey("UnitTest", true).join();
    assertThat(cloudKey1, notNullValue());
    byte[] encrypted = cloudKey1.encrypt(QUOTE.getBytes(UTF_8)).join();
    assertThat(encrypted, notNullValue());

    byte[] decrypted = cloudKey1.decrypt(encrypted).join();
    assertThat(decrypted, notNullValue());
    assertThat(new String(decrypted, UTF_8), equalTo(QUOTE));

    // Verify getting the key again works
    CloudKey cloudKey2 = cloudKeyManager.getKey("UnitTest", false).join();
    assertThat(cloudKey2, notNullValue());
    decrypted = cloudKey2.decrypt(encrypted).join();
    assertThat(decrypted, notNullValue());
    assertThat(new String(decrypted, UTF_8), equalTo(QUOTE));
  }

//  @Test
//  @Order(2)
  public void testGigabyte() throws IOException  {

    CloudKeyManager cloudKeyManager = getHappyCloudKeyManger();
    CloudKey cloudKey = cloudKeyManager.getKey("UnitTest", true).join();

    out.println("Generating random data file");
    Path o = Paths.get(System.getProperty("java.io.tmpdir"), "cloudkey.gigabyte");
    RandomHelper.rfile(o.toFile(), 1024 * 1024 * 1024);
    out.println(String.format("Original:  %d bytes", o.toFile().length()));

    out.println("Encrypting data file");
    Path e = Paths.get(System.getProperty("java.io.tmpdir"), "cloudkey.encrypted");
    e.toFile().deleteOnExit();
    try (ByteChannel in = Files.newByteChannel(o, StandardOpenOption.READ);
         ByteChannel out = Files.newByteChannel(e, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW)) {
      cloudKey.encrypt(in, out).join();
    }
    out.println(String.format("Encrypted: %d bytes", e.toFile().length()));
    assertEquals(cloudKey.ciphertextLength((int)o.toFile().length()), e.toFile().length());

    out.println("Decrypting data file");
    Path d = Paths.get(System.getProperty("java.io.tmpdir"), "cloudkey.decrypted");
    d.toFile().deleteOnExit();
    try (ByteChannel in = Files.newByteChannel(e, StandardOpenOption.READ);
         ByteChannel out = Files.newByteChannel(d, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW)) {
      cloudKey.decrypt(in, out).join();
    }
    out.println(String.format("Decrypted: %d bytes", d.toFile().length()));
    // plaintextLength + IV (12 bytes) + tag (16 bytes)
    assertEquals(o.toFile().length() + 12 + 16, e.toFile().length());

    out.println("Checking digests");
    assertEquals(DigestHelper.sha1(o.toFile()), DigestHelper.sha1(d.toFile()));
  }

  @Test
  @Order(3)
  public void verifyWestSide() {
    CloudKeyManager cloudKeyManagerWest = CloudKeyManager.aws()
        .application("cloudkey-manager")
        .environment("unittest")
        .regions("us-west-2")
        .build();

    CloudKey cloudKeyWest = cloudKeyManagerWest.getKey("UnitTest", false).join();
    assertThat(cloudKeyWest, notNullValue());
    byte[] encrypted = cloudKeyWest.encrypt(QUOTE.getBytes(UTF_8)).join();
    assertThat(encrypted, notNullValue());
    byte[] decrypted = cloudKeyWest.decrypt(encrypted).join();
    assertThat(decrypted, notNullValue());
    assertThat(new String(decrypted, UTF_8), equalTo(QUOTE));
  }

  @Test
  @Order(4)
  public void verifyEastSide() {
    // Verify encrypt east side
    CloudKeyManager cloudKeyManagerEast = CloudKeyManager.aws()
        .application("cloudkey-manager")
        .environment("unittest")
        .regions("us-east-1")
        .build();

    CloudKey cloudKeyEast = cloudKeyManagerEast.getKey("UnitTest", false).join();
    assertThat(cloudKeyEast, notNullValue());
    byte[] encrypted = cloudKeyEast.encrypt(QUOTE.getBytes(UTF_8)).join();
    assertThat(encrypted, notNullValue());
    byte[] decrypted = cloudKeyEast.decrypt(encrypted).join();
    assertThat(decrypted, notNullValue());
    assertThat(new String(decrypted, UTF_8), equalTo(QUOTE));
  }

  @Test
  @Order(5)
  public void verifyKMS() {
    // Check for KMS in us-west-2 called cloudkey-manager-unittest
    KmsAsyncClient client = KmsAsyncClient.builder().region(Region.US_WEST_2).build();
    String keyId = AWSKMSHelper.findKeyId(client, "alias/cloudkey-manager-unittest").join();
    log.info("Key ID for cloudkey-manager-unittest-us-west-2 is " + keyId);
    assertThat(keyId, notNullValue());

    // Verify tags
    Map<String, String> tags = AWSKMSHelper.getTags(client, keyId).join();
    assertThat(tags.get("application"), equalTo("cloudkey-manager"));
    assertThat(tags.get("environment"), equalTo("unittest"));

    // Verify key rotation
    boolean keyRotationEnabled = AWSKMSHelper.getKeyRotationEnabled(client, keyId).join();
    assertThat(keyRotationEnabled, is(true));

    // Check for KMS in us-east-1 called cloudkey-manager-unittest
    client = KmsAsyncClient.builder().region(Region.US_EAST_1).build();
    keyId = AWSKMSHelper.findKeyId(client, "alias/cloudkey-manager-unittest").join();
    log.info("Key ID for cloudkey-manager-unittest-us-east-1 is " + keyId);
    assertThat(keyId, notNullValue());

    // Verify tags
    tags = AWSKMSHelper.getTags(client, keyId).join();
    assertThat(tags.get("application"), equalTo("cloudkey-manager"));
    assertThat(tags.get("environment"), equalTo("unittest"));

    // Verify key rotation
    keyRotationEnabled = AWSKMSHelper.getKeyRotationEnabled(client, keyId).join();
    assertThat(keyRotationEnabled, is(true));
  }

  @Test
  @Order(6)
  public void verifyS3() {
    S3AsyncClient s3client = S3AsyncClient.builder().region(Region.US_WEST_2).build();
    byte[] data = AWSS3Helper.getObject(s3client, "cloudkey-manager-unittest-us-west-2", "keys/unittest").join();
    assertThat(data, notNullValue());
    assertThat(data.length, equalTo(DEK_LENGTH));

    boolean versioned = AWSS3Helper.getBucketVersioningEnabled(s3client, "cloudkey-manager-unittest-us-west-2").join();
    assertThat(versioned, is(true));

    boolean blocked = AWSS3Helper.getPublicAccessBlocked(s3client, "cloudkey-manager-unittest-us-west-2").join();
    assertThat(blocked, is(true));

    s3client = S3AsyncClient.builder().region(Region.US_EAST_1).build();
    data = AWSS3Helper.getObject(s3client, "cloudkey-manager-unittest-us-east-1", "keys/unittest").join();
    assertThat(data, notNullValue());
    assertThat(data.length, equalTo(DEK_LENGTH));

    versioned = AWSS3Helper.getBucketVersioningEnabled(s3client, "cloudkey-manager-unittest-us-east-1").join();
    assertThat(versioned, is(true));

    blocked = AWSS3Helper.getPublicAccessBlocked(s3client, "cloudkey-manager-unittest-us-east-1").join();
    assertThat(blocked, is(true));
  }

  @Test
  @Order(7)
  public void testRotation() {
    CloudKeyManager cloudKeyManager = getHappyCloudKeyManger();

    CloudKey cloudKey1 = cloudKeyManager.getKey("UnitTest", false).join();
    long version = cloudKey1.getCurrentVersion().join();
    byte[] encrypted = cloudKey1.encrypt(QUOTE.getBytes(UTF_8)).join();
    cloudKey1.rotate();
    assertThat(cloudKey1.getCurrentVersion(), not(equalTo(version)));
    byte[] decrypted = cloudKey1.decrypt(encrypted).join();
    assertThat(new String(decrypted, UTF_8), equalTo(QUOTE));

    CloudKey cloudKey2 = cloudKeyManager.getKey("UnitTest", false).join();
    decrypted = cloudKey2.decrypt(encrypted).join();
    assertThat(new String(decrypted, UTF_8), equalTo(QUOTE));
  }

  @Test
  @Order(8)
  public void testDelete() {
    CloudKeyManager cloudKeyManager = getHappyCloudKeyManger();
    cloudKeyManager.deleteKey("UnitTest").join();

    boolean error = false;
    try {
      cloudKeyManager.getKey("UnitTest").join();
    } catch (CompletionException x) {
      error = true;
      assertThat(x.getCause() instanceof NoSuchKeyException, equalTo(true));
    }
    assertThat(error, equalTo(true));
  }

}
