# NetRadius Cloud Key Manager

This library provides an easy to use data encryption and key management using cloud services for key 
encryption keys and data encryption key storage.

## Downloads
Releases of this library are published  on [Maven Central](https://search.maven.org/).

 * Release Repository - https://repo.maven.apache.org/maven2/com/netradius/cloud-key-manager/
 * Staging Repository - https://oss.sonatype.org/content/groups/staging/com/netradius/cloud-key-manager/
 * Snapshot Repository - https://oss.sonatype.org/content/repositories/snapshots/com/netradius/cloud-key-manager/
 
## Java Compatibility

This project tests on Java LTS releases only. At present this library targets the Java 11 runtime.

## Bugs & Enhancement Requests

Please file any bugs or enhancements at https://bitbucket.org/netradius/cloud-key-manager/issues

## License
This project is licensed under the BSD 3-Clause License. See [LICENSE.txt](https://bitbucket.org/netradius/java-commons/src/master/LICENSE.txt)

## Quick Links
 * [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
 * [Amazon SDK Javadoc](https://sdk.amazonaws.com/java/api/latest/index.html?software/amazon/awssdk/services/kms/KmsAsyncClient.html)
 
